import Vue from 'vue'
import Router from 'vue-router'
import Courses from "../components/Courses"
import Cours from "../components/Cours"
import CoursEdit from "../components/CoursEdit"
import Welcome from "../components/Welcome"
import AllNews from "../components/AllNews"
import News from "../components/News"
import Admin from "../components/Admin"
import Profile from "../components/Profile"
import AddressBook from "../components/AddressBook"
import Birthday from "../components/Birthday"

Vue.use(Router)

export default new Router({
	mode: 'history',

  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome,
      props: true
    },
    {
      path: '/allnews/',
      name: 'AllNews',
      component: AllNews,
      props: true
    },
    {
      path: '/addressbook/',
      name: 'AddressBook',
      component: AddressBook,
      props: true
    },
    {
      path: '/birthday/',
      name: 'Birthday',
      component: Birthday,
      props: true
    },
    {
      path: '/allnews/:id',
      name: 'news',
      component: News,
      props: true
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      props: true
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      props: true
    },
    {
      path: '/courses/',
      name: 'Courses',
      component: Courses,
      props: true
    },
    {
      path: '/cours/:id',
      name: 'Cours',
      component: Cours,
      props: true
    },
    {
      path: '/cours/edit/:id',
      name: 'CoursEdit',
      component: CoursEdit,
      props: true
    }

  ]
})
